import axios from "axios";

function getContacto(id: number): void {
    axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
        .then(printContacto);
};

const printContacto = (response: {
    data: {
        id: number;
        name: string;
        username: string;
        email: string;
        address: {
            street: string;
            suite: string;
            city: string;
            zipcode: string;
            geo: {
                lat: string;
                lng: string;
            };
        };
        phone: string;
        website: string;
        company: {
            name: string;
            catchPhrase: string;
            bs: string;
        };
    }
}): void => {
    const {
        name,
    username,
    email,
    address: {
      street,
      suite,
      zipcode,
      geo: { lat, lng },
    },
    phone
    } = response.data;

    const ext = phone.split("x",2)

    console.log(`
    La persona ${name} con el nombre de usuario ${username} y correo electrónico ${email}
    vive en: ${street} ${suite} ${zipcode} con las coordenadas ${lat}, ${lng}.
    Puede contactarlo al teléfono: ${ext[0]}${ext[1] ? ` con extensión ${ext[1]}` : "."}
    `);
};

getContacto(1)